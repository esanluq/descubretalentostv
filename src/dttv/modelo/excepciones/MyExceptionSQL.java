package dttv.modelo.excepciones;

public class MyExceptionSQL extends Exception
{
	private static final long serialVersionUID = 1L;

	public MyExceptionSQL(String msg)
	{
		super(msg);
	}
}
