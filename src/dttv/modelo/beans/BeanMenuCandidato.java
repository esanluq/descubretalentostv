package dttv.modelo.beans;

import java.sql.SQLException;

import javax.sql.DataSource;

import dttv.ayudas.BeanFecha;
import dttv.ayudas.BeanSQL;

public class BeanMenuCandidato
{
	public String contenido;
	public BeanCandidato candidato;
	public BeanSQL beanSQL;
	
	/**
	 * Constructor por defecto.
	 */
	public BeanMenuCandidato()
	{
		contenido="";
	}

	public BeanMenuCandidato(DataSource DS)
	{
		beanSQL=new BeanSQL(DS);
		contenido="";
	}

	public void setCandidato(BeanCandidato beanCandidato)
	{
		candidato=beanCandidato;
	}
	
	public BeanCandidato getCandidato()
	{
		return candidato;
	}
	
	public String getContenido()
	{
		return contenido;
	}
	
	public void setContenido(String contenido)
	{
		this.contenido=contenido;
	}
	
	public void cargartAudicion() throws SQLException 
	{
		String contenido = null;
		BeanAudicion audicion=beanSQL.getAudicion(candidato.getUsuario());
		BeanFecha beanFecha=new BeanFecha();
		if(audicion!=null)
		{
			contenido= "<h3>Datos de la Audición:</h3>" +
						"Fecha: "+ beanFecha.converFechaLocal(audicion.getFecha()) + "<br/>" +
						"Localidad: " + audicion.getLocalidad()  + "<br/>" +
						"Lugar: " + audicion.getLugar();
		}
		else
		{
			contenido="El usuario aun no tiene establecida una audición";
		}
		this.contenido=contenido;
	}
}
