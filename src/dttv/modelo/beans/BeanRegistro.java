package dttv.modelo.beans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import dttv.ayudas.BeanSQL;
import dttv.modelo.excepciones.MyExceptionSQL;

/**
 * @author Esteban S.
 *
 */
@SuppressWarnings("serial")
public class BeanRegistro implements Serializable 
{
	//Propiedades de la clase
	// Datos asociados al usuario
	private BeanCandidato beanCandidato;
	// Es una cadena conteniendo el combobox de categorias 
	private String comboCategorias;
	private DataSource DS;
	private BeanSQL beanSQL;
	private String contenido;
	
	/**
	 * Constructor por defecto.
	 * @throws MyExceptionSQL 
	 * @throws SQLException 
	 */
	public BeanRegistro(DataSource DS) throws SQLException, MyExceptionSQL
	{
		beanSQL=new BeanSQL(DS);
		setBeanCandidato(new BeanCandidato());
		setComboCategorias();
		contenido="";
	}
	
	/**
	 *  Este método devuelve un combo de formulario relleno con
	 *  las distintas categorías disponibles en la Base de Datos
	 * @param rs
	 * @return
	 * @throws SQLException
	 * @throws MyExceptionSQL 
	 */
	private void setComboCategorias() throws SQLException, MyExceptionSQL 
	{
		ArrayList<String> categorias=null;
		categorias=beanSQL.getCategorias();
		int j=1; // La tabla de categorias comienza en el uno, y el arraylist en el cero
		String combo="<select id='categoria' name='categoria'>";
		for( int i = 0 ; i < categorias.size() ; i++ )
		{
			combo+="<option value='" + Integer.toString(j) + "'>" + categorias.get(i) + "</option>";
			j++;
		}
		
		combo+="</select>";
		this.comboCategorias=combo;
	}
	
	public BeanCandidato getBeanCandidato()
	{
		return beanCandidato;
	}

	public void setBeanCandidato(BeanCandidato beanCandidato)
	{
		this.beanCandidato = beanCandidato;
	}

	public String getComboCategorias()
	{
		return comboCategorias;
	}

	public void setComboCategorias(String comboCategorias)
	{
		this.comboCategorias = comboCategorias;
	}
	public DataSource getDS()
	{
		return DS;
	}

	public void setDS(DataSource dS)
	{
		DS = dS;
	}

	public void setContenido(String string)
	{
		this.contenido=string;
		
	}
	
	public String getContenido(String string)
	{
		return contenido;
		
	}
}