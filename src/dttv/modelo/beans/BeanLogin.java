/**
 * 
 */
package dttv.modelo.beans;

import java.io.Serializable;

/**
 * @author Eduardo A. Ponce
 * 
 */
@SuppressWarnings("serial")
public class BeanLogin implements Serializable
{
	// Propiedades de la clase
	// Datos asociados al usuario
	private String usuario;
	private String clave;
	private String errorUsuario;
	private String errorClave;

	// Indica si la instancia recoge los datos de un usuario
	private boolean encontrado = false;

	/**
	 * Constructor por defecto.
	 */
	public BeanLogin()
	{
		this.usuario = "";
		this.clave = "";
		this.errorClave="";
		this.errorUsuario="";
	}

	/**
	 * Constructor con par�metros
	 * 
	 * @return
	 */
	public void BeanUsuario(String usuario, String clave)
	{
		this.usuario = usuario;
		this.clave = clave;
	}

	/**
	 * @param login
	 *            the usuario to set
	 */
	public void setUsuario(String usuario)
	{
		this.usuario = usuario;
	}

	/**
	 * @return the login
	 */
	public String getUsuario()
	{
		return usuario;
	}

	/**
	 * @param clave
	 *            the clave to set
	 */
	public void setClave(String clave)
	{
		this.clave = clave;
	}

	/**
	 * @return the clave
	 */
	public String getClave()
	{
		return clave;
	}

	/**
	 * @return the errorUsuario
	 */
	public String getErrorUsuario()
	{
		return errorUsuario;
	}

	/**
	 * @param errorUsuario
	 *            the errorUsuario to set
	 */
	public void setErrorUsuario(String errorUsuario)
	{
		this.errorUsuario = errorUsuario;
	}

	/**
	 * @return the errorClave
	 */
	public String getErrorClave()
	{
		return errorClave;
	}

	/**
	 * @param errorClave
	 *            the errorClave to set
	 */
	public void setErrorClave(String errorClave)
	{
		this.errorClave = errorClave;
	}

	/**
	 * @return the encontrado
	 */
	public boolean isEncontrado()
	{
		return encontrado;
	}

}
