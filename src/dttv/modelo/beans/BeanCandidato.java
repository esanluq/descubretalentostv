package dttv.modelo.beans;

import java.io.Serializable;

/**
 * @author Esteban S.
 *
 */
@SuppressWarnings("serial")
public class BeanCandidato implements Serializable 
{
	//Propiedades de la clase
	// Datos asociados al usuario
	private String usuario;
	private String errorUsuario;
	private String clave;
	private String errorClave;
	private String nombre;
	private String apellidos;
	private String edad;
	private String ciudad;
	private String categoria;
	private String seleccionado;
	
	/**
	 * Constructor por defecto.
	 */
	public BeanCandidato()
	{
		this.usuario="";
		this.errorClave="";
		this.errorUsuario="";
		this.clave="";
		this.nombre="";
		this.apellidos="";
		this.edad="";
		this.ciudad="";
		this.categoria="";
		this.seleccionado="";
	}
	
	
	/**
	 * @return the nombre
	 */
	public String getNombre()
	{
		return nombre;
	}


	/**
	 * @return the apellidos
	 */
	public String getApellidos()
	{
		return apellidos;
	}



	/**
	 * @return the ciudad
	 */
	public String getCiudad()
	{
		return ciudad;
	}


	/**
	 * @return the categoria
	 */
	public String getCategoria()
	{
		return categoria;
	}


	/**
	 * @return the usuario
	 */
	public String getUsuario()
	{
		return usuario;
	}


	/**
	 * @return the clave
	 */
	public String getClave()
	{
		return clave;
	}


	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave)
	{
		this.clave = clave;
	}


	/**
	 * @return the seleccionado
	 */
	public String getSeleccionado()
	{
		return seleccionado;
	}


	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}


	/**
	 * @param apellidos the apellidos to set
	 */
	public void setApellidos(String apellidos)
	{
		this.apellidos = apellidos;
	}


	/**
	 * @param ciudad the ciudad to set
	 */
	public void setCiudad(String ciudad)
	{
		this.ciudad = ciudad;
	}


	/**
	 * @param categoria the categoria to set
	 */
	public void setCategoria(String categoria)
	{
		this.categoria = categoria;
	}


	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario)
	{
		this.usuario = usuario;
	}


	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(String seleccionado)
	{
		this.seleccionado = seleccionado;
	}

	/**
	 * @return the errorUsuario
	 */
	public String getErrorUsuario()
	{
		return errorUsuario;
	}


	/**
	 * @param errorUsuario the errorUsuario to set
	 */
	public void setErrorUsuario(String errorUsuario)
	{
		this.errorUsuario = errorUsuario;
	}


	/**
	 * @return the errorClave
	 */
	public String getErrorClave()
	{
		return errorClave;
	}


	/**
	 * @param errorClave the errorClave to set
	 */
	public void setErrorClave(String errorClave)
	{
		this.errorClave = errorClave;
	}

	/**
	 * @return the edad
	 */
	public String getEdad()
	{
		return edad;
	}


	/**
	 * @param edad the edad to set
	 */
	public void setEdad(String edad)
	{
		this.edad = edad;
	}
	
	public String toString()
	{
		String contenido="<p>Los datos de <strong>"+ this.getNombre() +"</strong>"
				+ " son los siguientes:</p>" +
				"Usuario: " + this.getUsuario() +"<br/>"+
				"Clave: "+ this.getClave() +"<br/>"+
				"Nombre: "+ this.getNombre() +"<br/>"+
				"Apellidos: " + this.getApellidos() +"<br/>"+
				"Edad: " + this.getEdad() +"<br/>"+
				"Ciudad: " + this.getCiudad() +"<br/>"+
				"Categoría: " + this.getCategoria() +"<br/>" +
				"Seleccionado: " + this.getSeleccionado();
		return contenido;
	}
	
}