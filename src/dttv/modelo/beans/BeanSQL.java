package dttv.modelo.beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.sql.DataSource;

import com.sun.org.apache.xalan.internal.xsltc.runtime.Hashtable;

import dttv.ayudas.BeanFecha;
import dttv.modelo.excepciones.ErrorManejadorFecha;
import dttv.modelo.excepciones.MyExceptionSQL;


public class BeanSQL
{
	private DataSource DS=null;
	
	public BeanSQL(DataSource DS)
	{
		this.DS=DS;
	}
	
	public boolean existeUsuario(String usuario) throws SQLException
	{
		boolean existe = true;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = "select * from usuarios where usuario = '"+ usuario + "'";
		rs = st.executeQuery(sql);
		if (!rs.next())
		{
			existe=false;
		}
		conexion.close();
		return existe;
	}
	/**
	 * Si existe el usuario y la clave, devuelve el campo nivel de acceso
	 *  si no existe devuelve un null
	 * 
	 * @param usuario
	 * @param clave
	 * @return
	 * @throws SQLException
	 */
	public String loginOk(String usuario,String clave) throws SQLException
	{
		String nivel = null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select nivel from usuarios "
				+ "where usuario = '"+ usuario + "' and clave ='"+ clave +"'";
		rs = st.executeQuery(sql);
		if (rs.next())
		{
			nivel=rs.getString("nivel");
		}
		conexion.close();
		return nivel;
	}
	
	public boolean insertUsuario(String usuario,String clave) throws SQLException
	{
		boolean ok = true;
		Connection conexion = null;
		Statement st = null;
		int inserciones = 0;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"INSERT INTO usuarios (usuario,clave) values ('"+ usuario + "','"+ clave +"')";
		inserciones = st.executeUpdate(sql);
		if (inserciones==0)
		{
			ok=false;
		}
		conexion.close();
		return ok;
	}
	
	public boolean insertCandidato(BeanCandidato candidato) throws SQLException
	{
		boolean ok = true;
		Connection conexion = null;
		Statement st = null;
		int inserciones = 0;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql="INSERT INTO `candidatos` (`nombre`,`apellidos`,`edad`,`ciudad`,"
				+ "`categoria`,`usuario`) "
				+ "VALUES ('"+candidato.getNombre()+"',"
						+ "'"+candidato.getApellidos()+"',"
						+ Integer.parseInt(candidato.getEdad())+","
						+ "'"+candidato.getCiudad()+"',"
						+ Integer.parseInt(candidato.getCategoria())+","
						+ "'"+candidato.getUsuario()+"')";
		inserciones = st.executeUpdate(sql);
		if (inserciones==0)
		{
			ok=false;
		}
		conexion.close();
		return ok;
	}

	public ArrayList<String> getCategorias() throws SQLException, MyExceptionSQL
	{
		ArrayList<String> categorias=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;	
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql="select * from categorias";
		rs = st.executeQuery(sql);
		
		if (rs!=null)
		{	categorias=new ArrayList<String>();
			while (rs.next())
			{
				 categorias.add(rs.getString("descripcion"));
			}
		}
		else
		{
			throw new MyExceptionSQL("No existe ninguna categoría "
					+ "a la que poder presentarse");
		}
		conexion.close();
		return categorias;
	}

	public BeanAudicion getAudicion(String usuario) throws SQLException
	{
		BeanAudicion audicion=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select fecha,localidad,lugar from audicion "
				+ "where candidato = '"+ usuario + "'";
		rs = st.executeQuery(sql);
		if (rs.next())
		{
			audicion=new BeanAudicion(rs.getDate("fecha"), rs.getString("localidad"), rs.getString("lugar"));
		}
		conexion.close();
		return audicion;
	}

	public BeanCandidato getCandidato(String usuario) throws SQLException
	{
		BeanCandidato candidato=new BeanCandidato();
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select a.*,b.clave from candidatos a, usuarios b "
				+ "where a.usuario=b.usuario and a.usuario = '"+ usuario + "'";
		rs = st.executeQuery(sql);
		if (rs.next())
		{
			candidato.setNombre(rs.getString("nombre"));
			candidato.setApellidos(rs.getString("apellidos"));
			candidato.setEdad(Integer.toString(rs.getInt("edad")));
			candidato.setCiudad(rs.getString("ciudad"));
			candidato.setCategoria(Integer.toString(rs.getInt("categoria")));
			candidato.setUsuario(rs.getString("usuario"));
			candidato.setClave(rs.getString("clave"));
			candidato.setSeleccionado(rs.getString("seleccionado"));
		}
		conexion.close();
		return candidato;
	}

	public HashMap<String,String> getSeleccionados(String seleccionado) throws SQLException
	{
		HashMap<String,String> mapa=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select usuario,nombre,apellidos from candidatos "
				+ "where seleccionado = '"+seleccionado+"'";
		rs = st.executeQuery(sql);
		if (rs!=null)
		{
			mapa=new HashMap<String,String>();
			while (rs.next())
			{
				String nombreCompleto=rs.getString("nombre") + " "+ rs.getString("apellidos");
				mapa.put(rs.getString("usuario"),nombreCompleto);
			}
		}
		return mapa;
	}

	public HashMap<String, String> getEvaluados(String evaluado) throws SQLException
	{
		HashMap<String,String> mapa=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select usuario,nombre,apellidos from candidatos "
				+ "where evaluado = '"+evaluado+"'";
		rs = st.executeQuery(sql);
		if (rs!=null)
		{
			mapa=new HashMap<String,String>();
			while (rs.next())
			{
				String nombreCompleto=rs.getString("nombre") + " "+ rs.getString("apellidos");
				mapa.put(rs.getString("usuario"),nombreCompleto);
			}
		}
		return mapa;
	}

	public boolean insertPeriodoRegistro(String fechaI, String fechaF) throws SQLException, ErrorManejadorFecha
	{
		boolean ok = true;
		BeanFecha beanFecha=new BeanFecha();
		Connection conexion = null;
		Statement st = null;
		int inserciones = 0;
		String sql;
		conexion = DS.getConnection();
		
		// Borro lo que hubiera en la tabla de `periodoregistro`
		st = conexion.createStatement();
		sql="Delete FROM periodoregistro";
		st.executeUpdate(sql);
		
		// Despues convierto las fechas a cadenas MySQL e inserto
		Date fecha1=beanFecha.parseaFecha(fechaI);
		Date fecha2=beanFecha.parseaFecha(fechaF);
		fechaI=beanFecha.converUtilDateToSqlString(fecha1);
		fechaF=beanFecha.converUtilDateToSqlString(fecha2);
		sql="INSERT INTO `periodoregistro` (`fechaini`,`fechafin`) "
				+ "VALUES ('"+fechaI+"','"+fechaF+"')";
		inserciones = st.executeUpdate(sql);
		if (inserciones==0)
		{
			ok=false;
		}
		conexion.close();
		return ok;
	}
}
