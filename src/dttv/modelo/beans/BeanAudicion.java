package dttv.modelo.beans;

import java.util.Date;

/**
 *  Clase POJO que maneja tres cadenas con los datos de
 *  fecha, localidad y audicion
 *  
 * @author sanchezesteban69j
 *
 */
public class BeanAudicion
{
	private Date fecha;
	private String localidad;
	private String lugar;
	
	public BeanAudicion(Date fecha, String localidad, String lugar)
	{
		this.fecha=fecha;
		this.localidad=localidad;
		this.lugar=lugar;
	}
	
	/**
	 * @return the fecha
	 */
	public Date getFecha()
	{
		return fecha;
	}

	/**
	 * @return the localidad
	 */
	public String getLocalidad()
	{
		return localidad;
	}

	/**
	 * @return the lugar
	 */
	public String getLugar()
	{
		return lugar;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha)
	{
		this.fecha = fecha;
	}

	/**
	 * @param localidad the localidad to set
	 */
	public void setLocalidad(String localidad)
	{
		this.localidad = localidad;
	}

	/**
	 * @param lugar the lugar to set
	 */
	public void setLugar(String lugar)
	{
		this.lugar = lugar;
	}

}
