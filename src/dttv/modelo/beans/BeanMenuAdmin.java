package dttv.modelo.beans;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import dttv.ayudas.BeanFecha;
import dttv.ayudas.BeanSQL;
import dttv.modelo.excepciones.ErrorManejadorFecha;
import dttv.modelo.excepciones.MyExceptionSQL;

/**
 * Clase que se encarga de realizar operaciones necesarias
 * para el funcionamiento del menú de opciones del perfil de 
 * administrador
 * 
 * @author Esteban S.
 *
 */
public class BeanMenuAdmin
{
	private String contenido;
	private BeanCandidato candidato;
	private BeanSQL beanSQL;
	private String fechaI,fechaF;
	private String msgFecha,msgEvaluar,msgAudicion;

	/**
	 * Constructor que recibe un DataSource como parámetro
	 */
	public BeanMenuAdmin(DataSource DS)
	{
		beanSQL = new BeanSQL(DS);
		msgFecha="";
		msgEvaluar="";
		contenido="";
		fechaI="";
		fechaF="";
	}

	public void setFechaI(String fechaI)
	{
		this.fechaI = fechaI;
	}

	public void setFechaF(String fechaF)
	{
		this.fechaF = fechaF;
	}

	public void setCandidato(BeanCandidato beanCandidato)
	{
		candidato = beanCandidato;
	}

	public BeanCandidato getCandidato()
	{
		return candidato;
	}

	public String getContenido()
	{
		return contenido;
	}

	public void setContenido(String contenido)
	{
		this.contenido=contenido;
	}

	public String getMsgFecha()
	{
		return msgFecha;
	}

	public void setMsgFecha(String msgFecha)
	{
		this.msgFecha = msgFecha;
	}

	/**
	 * Se encarga de guardar las fechas del periodo de registro
	 * de candidatos
	 * 
	 * @param fechaI	String
	 * @param fechaF	String
	 * @throws SQLException
	 * @throws ErrorManejadorFecha
	 */
	public void insertarPeriodoRegistro(String fechaI,String fechaF) 
			throws SQLException, ErrorManejadorFecha
	{			
		if(!beanSQL.insertPeriodoRegistro(fechaI,fechaF))
		{
			this.msgFecha="No se guardaron las fechas";
		}
	}

	/**
	 * Se encarga de establecer la propiedad <contenido>  con 
	 * los datos de los usuarios seleccionados para el concurso
	 *  
	 * @throws SQLException
	 */
	public void cargarSeleccionados() throws SQLException
	{
		String contenido = null;
		HashMap<String,String> candidatos = beanSQL.getSeleccionados("S");
		Iterator<Entry<String, String>> it = candidatos.entrySet().iterator();
		if(candidatos.size()>0)
		{
			contenido="<H3>Candidatos Seleccionados:</h3>";
			contenido+="<ul>";
			while (it.hasNext()) 
			{
				Map.Entry<String,String> e = (Entry<String, String>)it.next();
	            contenido+="<li>"
	            + "<a href='#' onclick=\"verPerfil('"+e.getKey()+"');\">"+
	            		e.getValue()+"</a></li>";
			}
			contenido+="</ul>";
		}
		else
		{
			contenido = "<br/>No existen candidatos seleccionados!";
		}
		this.contenido=contenido;
	}

	/**
	  * Se encarga de establecer la propiedad <contenido>  con 
	 * los datos de los usuarios que no han sido seleccionados 
	 * para el concurso
	 * @throws SQLException
	 */
	public void cargarNoSeleccionados() throws SQLException
	{
		String contenido = null;
		HashMap<String,String> candidatos = beanSQL.getSeleccionados("N");
		Iterator<Entry<String, String>> it = candidatos.entrySet().iterator();
		if(candidatos.size()>0)
		{
			contenido="<H3>Candidatos No Seleccionados:</h3>";
			contenido+="<ul>";
			while (it.hasNext()) 
			{
				Map.Entry<String,String> e = (Entry<String, String>)it.next();
	            contenido+="<li>"
	            + "<a href='#' onclick=\"verPerfil('"+e.getKey()+"');\">"+
	            		e.getValue()+"</a></li>";
			}
			contenido+="</ul>";
		}
		else
		{
			contenido = "<br/>No existen candidatos no seleccionados!";
		}
		this.contenido=contenido;
	}

	/**
	 * Se encarga de establecer la cadena <contenido> con los candidatos
	 * pendientes de evaluar
	 * 
	 * @throws SQLException
	 */
	public void cargarPendientesEval() throws SQLException
	{
		String contenido = null;
		HashMap<String,String> candidatos = beanSQL.getEvaluados("N");
		Iterator<Entry<String, String>> it = candidatos.entrySet().iterator();
		if(candidatos.size()>0)
		{
			contenido="<H3>Candidatos Pendientes de Evaluar:</h3>";
			contenido+="<ul>";
			while (it.hasNext()) 
			{
				Map.Entry<String,String> e = (Entry<String, String>)it.next();
	            contenido+="<li>"
	            + "<a href='#' onclick=\"verPerfil('"+e.getKey()+"');\">"+
	            		e.getValue()+"</a></li>";
			}
			contenido+="</ul>";
		}
		else
		{
			contenido = "<br/>No existen candidatos no seleccionados!";
		}
		this.contenido=contenido;
	}

	/**
	 * Se encarga de establecer <contenido> con el formulario usado
	 * para establecer el periodo de registro de candidatos
	 * 
	 * @return
	 */
	public void cargarFormPeriodoReg()
	{
		String contenido;
		contenido="<p>Introduzca el periodo de fechas, en que "+
					"pueden <cite>registrarse candidatos</cite>:</p>";
		if(!msgFecha.equals(""))
		{
			contenido+="<p>"+msgFecha+"</p>";
		}
		contenido+="<div style='padding: 10px;'>"
				+ "Fecha Inicial (dd/MM/yyyy): ";
		contenido+="<input type='text' name='fechaI' size='13' value='"+this.fechaI+"'/><br/><br/>";
		contenido+="Fecha final (dd/MM/yyyy): ";
		contenido+="<input type='text' name='fechaF' size='13' value='"+this.fechaF+"'/><br/></br/>"
				+ "<span style='margin-left: 10%;'>";
		contenido+="<a href='#' class='botonLogin' onclick=\"submitAction('setPeriodoRegistro');\">Guardar</a>";
		contenido+="<input type='reset' class='botonLogin' value='Cancelar'/>"
				+ "</span></div>";
		this.contenido=contenido;
	}

	public void cargarCategorias() throws SQLException, MyExceptionSQL
	{
		String contenido = null;
		ArrayList<String> categorias = beanSQL.getCategorias();

		if (categorias!=null)
		{
			contenido="<H3>Categorías:</h3>";
			for( int i = 0 ; i < categorias.size() ; i++ )
			{
				contenido+=categorias.get(i) + "<br/>";
			}
		}
		else
		{
			contenido = "No existen categorías";
		}
		this.contenido=contenido;
	}

	/**
	 * Se encarga de validar dos fechas que se le pasan como argumento
	 *  
	 * @param fechaI  String
	 * @param fechaF	String
	 * @return  ok 	Booleano
	 */
	public boolean validaFechas(String fechaI, String fechaF)
	{
		boolean ok=true;
		String msgFecha="";
		BeanFecha beanFecha=new BeanFecha();
		try
		{
			//Date fecha1;
			beanFecha.parseaFecha(fechaI);
		}
		catch (ErrorManejadorFecha e)
		{
			msgFecha="Las primera fecha no es correcta.<br/>";
			ok=false;
		}
		try
		{
			//Date fecha2;
			beanFecha.parseaFecha(fechaF);
		}
		catch (ErrorManejadorFecha e)
		{
			msgFecha+="Las segunda fecha no es correcta<br/>";
			ok=false;
		}
		/*
		// Ahora valido que la primera fecha sea mayor o igual a hoy
		// y que la primera no sea mayor que la segunda
		if(ok)
		{
			Date hoy=new Date();
			if(!beanFecha.esMayorIgual(fecha1, hoy))
			{
				msgFecha+="La fecha inicial debe ser mayor o igual a hoy<br/>";
				ok=false;
			}
			if(beanFecha.esMayorIgual(fecha1, fecha2))
			{
				msgFecha+="Las segunda fecha no puede ser menor que la primera";
				ok=false;
			}	
		}
		*/
		this.msgFecha=msgFecha;
		return ok;
	}

	/**
	 * Establece el valor de la propiedad msgEvaluar
	 * que aparecerá en el formulario de evaluación de candidatos
	 * @param msg
	 */
	public void setMsgEvaluar(String msg)
	{
		this.msgEvaluar=msg;
		
	}
	
	/**
	 * 	Se encarga de guardar en la base de datos
	 * el resultado de la evaluación (seleccionado, o no seleccionado)
	 * de un candidato
	 * @param  candidato  Un objeto de tipo BeanCandidato
	 * @throws MyExceptionSQL 
	 * @throws SQLException 
	 */
	public void evalCandidato()
			throws SQLException, MyExceptionSQL
	{
		beanSQL.updEvalCandidato(this.candidato);
		
	}
	
	/**
	 *   Se encarga de crear el formulario que se usará para 
	 *   seleccionar a un candidato con audición asignada
	 *   que no ha sido evaluado aun
	 * @throws SQLException 
	 *   
	 */

	public void cargaFormNoEvaluadosConAudicio() throws SQLException
	{
		String contenido = null;
		HashMap<String,String> candidatos = beanSQL.getNoEvaluadosConAudicion();
		Iterator<Entry<String, String>> it = candidatos.entrySet().iterator();
		if(candidatos.size()>0)
		{
			contenido="<H3>Candidatos Pendientes de Evaluar:</h3>";
			contenido+="<ul>";
			while (it.hasNext()) 
			{
				Map.Entry<String,String> e = (Entry<String, String>)it.next();
	            contenido+="<li>"
	            + e.getValue() 
	            + "Seleccionado: Si <input name='seleccionado' value='S' type='radio'></li>"
	            + "	No <input checked='checked' name='seleccionado' value='N' type='radio'>"
	            +"<a href='#' class='botonLogin' onclick=\"evalCandidato('"+e.getKey()+"');\"/>"
	            +"Evaluar</a>";
			}
			contenido+="</ul>";
		}
		else
		{
			contenido = "<br/>No existen candidatos con audición asignada!";
		}
		this.contenido=contenido;
		
	}

	/**
	 * Se encarga de establecer los datos de audición 
	 * de un usuario, abstrayendo la Base de Datos
	 * 
	 * @param audicion  BeanAudicion
	 * @throws SQLException
	 */
	public void setAudicion(BeanAudicion audicion) 
			throws SQLException
	{
		beanSQL.setAudicion(candidato.getUsuario(),audicion);
		
	}

	/**
	 *   Se encarga de crear el formulario que se usará para 
	 *   seleccionar a un candidato no evaluado aun
	 * @throws SQLException 
	 *   
	 */

	public void cargaForm1SetAudicion() throws SQLException
	{
		String contenido = null;
		HashMap<String,String> candidatos = beanSQL.getCandidatosSinAudicion();
		Iterator<Entry<String, String>> it = candidatos.entrySet().iterator();
		if(candidatos.size()>0)
		{
			contenido="<H3>Pique sobre el candidato para asignar:</h3>";
			contenido+="<ul>";
			while (it.hasNext()) 
			{
				Map.Entry<String,String> e = (Entry<String, String>)it.next();
	            contenido+="<li>"
	            		+"<a href='#' onclick=\"formSetAudicion('"+e.getKey()+"');\"/>"
	            		+ e.getValue() +"</a></li>";
			}
			contenido+="</ul>";
		}
		else
		{
			contenido = "<br/>No existen candidatos con audición asignada!";
		}
		this.contenido=contenido;
		
	}

	/**
	 * Crea el formulario de inserción de los datos de una audición
	 * 
	 * @throws MyExceptionSQL 
	 * @throws SQLException 
	 */
	public void cargaForm2SetAudicion() 
			throws SQLException, MyExceptionSQL
	{
		String contenido;
		HashMap<Integer, String> localidades;
		ArrayList<String> lugares;
		Iterator<Entry<Integer, String>> it;
		Map.Entry<Integer,String> e;
		
		contenido="<H3>Seleccione la audición para "+candidato.getNombre()+"</h3>";
		contenido+="Fecha: (dd/mm/yyyy):<input type='text' name='fecha' /><br/><br/>";
		contenido+="Localidades: "+
				"<select name='localidades' OnChange='cambiar()'>";
		
		// Aqui comienza el bucle de elementos padres
		localidades = beanSQL.getLocalidades();
		it = localidades.entrySet().iterator();
		while (it.hasNext()) 
		{
			e = (Entry<Integer, String>)it.next();
			contenido+="<option value='"+e.getKey().toString()+"'>"+ e.getValue() +"</option>";
		}
		contenido+="</select><br/><br/>";

		// Aquí va el segundo select vacío
		contenido+= "Lugares: <select name='lugares'>";
		it = localidades.entrySet().iterator();
		it.hasNext();
		e = (Entry<Integer, String>)it.next();
		lugares=beanSQL.getLugares(e.getKey());
		for( int i = 0 ;i < lugares.size() ; i++ )
		{
			contenido+="<option value='"+lugares.get(i)+"'>"+ lugares.get(i) +"</option>";
		}
		contenido+="</select><br/><br/>";
		
		// Aquí comienzan las funciones javascript
		contenido+="<script>";
				
		it = localidades.entrySet().iterator();
		int i=0;
		while (it.hasNext()) 
		{
			// Aquí debe ir el bucle de los elementos hijos
			contenido+="function lugares"+Integer.toString(i)+"() {";
			e = (Entry<Integer, String>)it.next();
			lugares=beanSQL.getLugares(e.getKey());			
			for( int j = 0 ; j < lugares.size() ; j++ )
			{
				contenido+="opcion"+Integer.toString(i)+"=new Option('"+lugares.get(j)+"','"+lugares.get(j)+"');";
			}
			contenido+="document.formu.lugares.options[0]=opcion"+Integer.toString(i)+";"; 
			i++;
			contenido+="}";
		}
					
		// Aquí comienzan las funcion cambiar()
		contenido+="function cambiar()	{"
				+"var index=document.formu.localidades.selectedIndex;"							
				+"document.formu.lugares.length=0;";
		
		// Aquí va otro bucle
		for( int i1 = 0 ; i1 < localidades.size() ; i1++ )
		{
			contenido+="if(index=="+Integer.toString(i1)+") lugares"+Integer.toString(i1)+"();"; 
		}
		contenido+="}";
		contenido+= "</script>";
		contenido+="<a href='#' class='botonLogout' onclick=\"setAudicion('"+candidato.getUsuario()+"');\"/>"
        		+"Enviar</a>";
		contenido+="<a href='#' class='botonLogout' onclick='reset();'/>"+"Cancelar</a>";
		this.contenido=contenido;
	}

	/**
	 * Establece la propiedad msgAudicion, para mostrar mensajes de error
	 * o informativos en el formulario de establecimiento de una Audicion
	 * para un candidato
	 * 
	 * @param msgAudicion  Cadena con un mensaje
	 */
	public void setMsgAudicion(String msgAudicion)
	{
		this.msgAudicion = msgAudicion;
	}

}
