package dttv.modelo.acciones;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import dttv.ayudas.PathVistas;
import dttv.controlador.Accion;
import dttv.modelo.beans.BeanCandidato;
import dttv.modelo.beans.BeanError;
import dttv.modelo.beans.BeanMenuAdmin;
import dttv.modelo.excepciones.MyExceptionSQL;

/**
 * 
 * 
 * @author Esteban S.
 */
public class AccionVerCategorias implements Accion
{
	// Estas variables las necesitan todas las acciones
	private String vista;
	private Object modelo;
	private ServletContext sc;
	private HttpSession sesion;
	private DataSource DS;
	private BeanMenuAdmin beanMenuAdmin;
	private BeanCandidato candidato;

	/**
	 * Constructor por defecto
	 */
	public AccionVerCategorias()
	{
	}

	/**
	 * Ejecuta el proceso asociado a la acción.
	 * 
	 * @param request    Objeto que encapsula la petición.
	 * @param response   Objeto que encapsula la respuesta.
	 * @return true o false en función de que no se hayan producido errores o lo
	 *         contrario.
	 * @see dttv.controlador.Accion#ejecutar(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	public boolean ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		boolean resultado = true;
		candidato= (BeanCandidato) sesion.getAttribute("candidato");
		BeanMenuAdmin beanMenuAdmin=new BeanMenuAdmin(DS);
		beanMenuAdmin.setCandidato(candidato);
		try
		{
			beanMenuAdmin.cargarCategorias();
			modelo=beanMenuAdmin;
			vista=PathVistas.ADMINISTRADOR;
		}
		catch (SQLException e)
		{
			modelo = new BeanError(1, "Error en conexión a base de datos", e);
			vista=PathVistas.GESERROR;
			resultado = false;
		}
		catch (MyExceptionSQL e)
		{
			modelo = new BeanError(1004, "N/A", e);
			vista=PathVistas.GESERROR;
			resultado = false;
		}
		return resultado;
	}

	/**
	 * Devuelve la vista actual.
	 * 
	 * @param La
	 *            vista a devolver al usuario.
	 */
	public String getVista()
	{
		return vista;
	}

	/**
	 * Establece la vista
	 * 
	 * @param vista   Página que se enviará al usuario
	 */
	public void setVista(String vista)
	{
		this.vista = vista;
	}

	/**
	 * Devuelve el modelo con el que trabajará la vista.
	 * 
	 * @return El modelo a procesar por la vista.
	 */
	public Object getModelo()
	{
		return modelo;
	}

	/**
	 * Establece el modelo con el que trabajará la vista.
	 * 
	 * @param modelo   Conjunto de datos a procesar por la vista.
	 */
	public void setModelo(Object modelo)
	{
		this.modelo = modelo;
	}

	/**
	 * Establece el contexto del servlet (nivel aplicación)
	 * 
	 * @param sc
	 *            Objeto ServletContext que encapsula el ámbito de aplicación.
	 */
	public void setSc(ServletContext sc)
	{
		this.sc = sc;
	}

	/**
	 * Devuelve el contexto del servlet (nivel aplicación)
	 * 
	 * @return Objeto que encapsula el nivel de aplicación.
	 */
	public ServletContext getSc()
	{
		return sc;
	}

	/**
	 * Establece el objeto que encapsula la sesión actual.
	 * 
	 * @param sesion
	 *            La sesión a establecer en la acción.
	 */

	public void setSesion(HttpSession sesion)
	{
		this.sesion = sesion;
	}

	/**
	 * Devuelve la sesión actual del usuario.
	 * 
	 * @return sesion La sesión actual del usuario.
	 */

	public HttpSession getSesion()
	{
		return sesion;
	}

	/**
	 * Establece el datasource con el que se trabajará
	 * 
	 * @param ds
	 *            Datasource
	 */
	public void setDS(DataSource ds)
	{
		DS = ds;
	}

	@Override
	public BeanError getError()
	{
		// TODO Auto-generated method stub
		return (BeanError) modelo;
	}
}


