/**
 * Acción: procesar login usuario
 */
package dttv.modelo.acciones;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import dttv.ayudas.BeanSQL;
import dttv.ayudas.PathVistas;
import dttv.controlador.Accion;
import dttv.modelo.beans.BeanCandidato;
import dttv.modelo.beans.BeanError;
import dttv.modelo.beans.BeanLogin;
import dttv.modelo.beans.BeanMenuAdmin;
import dttv.modelo.beans.BeanMenuCandidato;

/**
 * Procesa los datos de login y clave proporcionados por un usuario.
 * 
 * @author Eduardo A. Ponce
 */

public class AccionValidarLogin implements Accion
{

	// Estas variables las necesitan todas las acciones
	private String vista;
	private Object modelo;
	private ServletContext sc;
	private HttpSession sesion;
	private DataSource DS;
	private BeanSQL beanSQL;

	/**
	 * Constructor por defecto
	 */
	public AccionValidarLogin()
	{
	}

	/**
	 * Ejecuta el proceso asociado a la acción.
	 * 
	 * @param request    Objeto que encapsula la petición.
	 * @param response   Objeto que encapsula la respuesta.
	 * @return true o false en función de que no se hayan producido errores o lo
	 *         contrario.
	 * @see dttv.controlador.Accion#ejecutar(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	public boolean ejecutar(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		boolean resultado=true;
		String usuario, clave;
		// Si la accion es login, se valida el login.
		// Se obtienen login y clave
		usuario = request.getParameter("usuario").trim();
		clave = request.getParameter("clave").trim();
		resultado=validaLogin(usuario,clave);
		return resultado;
	}
	
	private boolean validaLogin(String usuario,String clave)
	{
		boolean resultado = true;
		BeanLogin login=new BeanLogin();
		String nivel=null;
		beanSQL=new BeanSQL(DS);
		login.setUsuario(usuario);
		login.setClave(clave);
		try
		{
			if (beanSQL.existeUsuario(usuario))
			{
				nivel=beanSQL.loginOk(usuario, clave);
				if (nivel==null)
				{
					login.setErrorClave("Clave incorrecta.");
					vista = PathVistas.LOGIN;
					modelo=login;
				}
				else
				{
					if(nivel.equals("candidato"))
					{
						vista = PathVistas.CANDIDATO;
						modelo=new BeanMenuCandidato(DS);
						BeanCandidato candidato=beanSQL.getCandidato(usuario);
						sesion.setAttribute("candidato", candidato);
					}
					else
					{
						vista = PathVistas.ADMINISTRADOR;
						modelo=new BeanMenuAdmin(DS);
					}
					sesion.setAttribute("login", "OK" );
					sesion.setAttribute("usuario", usuario);
				}
			}
			else
			{
				login.setErrorUsuario("El usuario no existe.");
				vista = PathVistas.LOGIN;
				modelo=login;
			}
		}
		catch (SQLException e)
		{
			modelo = new BeanError(1, "Error en conexión a BD", e);
			vista=PathVistas.GESERROR;
			resultado = false;
		}
		return resultado;
	}
	/**
	 * Devuelve la vista actual.
	 * 
	 * @param La
	 *            vista a devolver al usuario.
	 */
	public String getVista()
	{
		return vista;
	}

	/**
	 * Establece la vista
	 * 
	 * @param vista   Página que se enviará al usuario
	 */
	public void setVista(String vista)
	{
		this.vista = vista;
	}

	/**
	 * Devuelve el modelo con el que trabajará la vista.
	 * 
	 * @return El modelo a procesar por la vista.
	 */
	public Object getModelo()
	{
		return modelo;
	}

	/**
	 * Establece el modelo con el que trabajará la vista.
	 * 
	 * @param modelo   Conjunto de datos a procesar por la vista.
	 */
	public void setModelo(Object modelo)
	{
		this.modelo = modelo;
	}

	/**
	 * Establece el contexto del servlet (nivel aplicación)
	 * 
	 * @param sc
	 *            Objeto ServletContext que encapsula el ámbito de aplicación.
	 */
	public void setSc(ServletContext sc)
	{
		this.sc = sc;
	}

	/**
	 * Devuelve el contexto del servlet (nivel aplicación)
	 * 
	 * @return Objeto que encapsula el nivel de aplicación.
	 */
	public ServletContext getSc()
	{
		return sc;
	}

	/**
	 * Establece el objeto que encapsula la sesión actual.
	 * 
	 * @param sesion
	 *            La sesión a establecer en la acción.
	 */

	public void setSesion(HttpSession sesion)
	{
		this.sesion = sesion;
	}

	/**
	 * Devuelve la sesión actual del usuario.
	 * 
	 * @return sesion La sesión actual del usuario.
	 */

	public HttpSession getSesion()
	{
		return sesion;
	}

	/**
	 * Establece el datasource con el que se trabajará
	 * 
	 * @param ds
	 *            Datasource
	 */
	public void setDS(DataSource ds)
	{
		DS = ds;
	}

	@Override
	public BeanError getError()
	{
		// TODO Auto-generated method stub
		return (BeanError) modelo;
	}
}
