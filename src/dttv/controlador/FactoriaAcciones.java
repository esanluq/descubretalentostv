/**
 * Instancia objetos de tipo Acción.
 * Es una clase abstracta que impide que se puedan instanciar objetos de ella,
 * pero permite que se obtengan clases derivadas.
 * Se encarga de obtener los objetos Acción específicos para una determinada acción.
 */
package dttv.controlador;

import dttv.modelo.acciones.AccionEvalCandidato;
import dttv.modelo.acciones.AccionFormEvalCandidato;
import dttv.modelo.acciones.AccionFormSetAudicion;
import dttv.modelo.acciones.AccionIndex;
import dttv.modelo.acciones.AccionLogout;
import dttv.modelo.acciones.AccionFormRegistrar;
import dttv.modelo.acciones.AccionFormLogin;
import dttv.modelo.acciones.AccionSetAudicion;
import dttv.modelo.acciones.AccionSetPeriodoRegistro;
import dttv.modelo.acciones.AccionValidarLogin;
import dttv.modelo.acciones.AccionValidarRegistro;
import dttv.modelo.acciones.AccionVerAudicion;
import dttv.modelo.acciones.AccionVerCategorias;
import dttv.modelo.acciones.AccionVerFormPeriodoReg;
import dttv.modelo.acciones.AccionVerNoSeleccionados;
import dttv.modelo.acciones.AccionVerPendientesEval;
import dttv.modelo.acciones.AccionVerPerfil;
import dttv.modelo.acciones.AccionVerSeleccionados;

/**
 * Factoría que devuelve los objetos Accion que procesarán las peticiones
 * 
 * @author Eduardo A. Ponce
 * @version 2.0
 */
public abstract class FactoriaAcciones
{
	public static Accion creaAccion(String accion)
	{
		// Devuelve objetos Accion en función del parámetro de acción
		// proporcionado
		// Este código puede modificarse a libre elección
		Accion accionF=null;
		
		if (accion.equals("formLogin"))
		{
			accionF = new AccionFormLogin();
		}
		else if (accion.equals("validarLogin"))
		{
			accionF = new AccionValidarLogin();
		}
		else if (accion.equals("formRegistrar"))
		{
			accionF = new AccionFormRegistrar();
		}
		else if (accion.equals("validarRegistro"))
		{
			accionF = new AccionValidarRegistro();
		}
		else if (accion.equals("formSetAudicion"))
		{
			accionF = new AccionFormSetAudicion();
		}
		else if (accion.equals("setAudicion"))
		{
			accionF = new AccionSetAudicion();
		}
		else if (accion.equals("formEvalCandidato"))
		{
			accionF = new AccionFormEvalCandidato();
		}
		else if (accion.equals("evalCandidato"))
		{
			accionF = new AccionEvalCandidato();
		}
		else if (accion.equals("verSeleccionados"))
		{
			accionF = new AccionVerSeleccionados();
		}
		else if (accion.equals("verNoSeleccionados"))
		{
			accionF = new AccionVerNoSeleccionados();
		}
		else if (accion.equals("verPendientesEval"))
		{
			accionF = new AccionVerPendientesEval();
		}
		else if (accion.equals("verFormPeriodoReg"))
		{
			accionF = new AccionVerFormPeriodoReg();
		}
		else if (accion.equals("setPeriodoRegistro"))
		{
			accionF = new AccionSetPeriodoRegistro();
		}
		else if (accion.equals("verCategorias"))
		{
			accionF = new AccionVerCategorias();
		}
		else if (accion.equals("verPerfil"))
		{
			accionF = new AccionVerPerfil();
		}
		else if (accion.equals("verAudicion"))
		{
			accionF = new AccionVerAudicion();
		}
		else if (accion.equals("volver"))
		{
			accionF = new AccionIndex();
		}
		else if(accion.equals("logout"))
		{
			accionF = new AccionLogout();
		}
		else
		{
			// Acción por defecto. Conduce a index.jsp. Puede escogerse otra.
			accionF = new AccionIndex();
		}

		return accionF;
	}
}