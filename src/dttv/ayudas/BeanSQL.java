package dttv.ayudas;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import dttv.modelo.beans.BeanAudicion;
import dttv.modelo.beans.BeanCandidato;
import dttv.modelo.excepciones.ErrorManejadorFecha;
import dttv.modelo.excepciones.MyExceptionSQL;

/**
 * Clase que se encarga de hacer consultas a la Base de Datos
 * 
 * @author Esteban S.
 *
 */
public class BeanSQL
{
	/**
	 *  Propiedad DataSource que se usará para obtener una conexión a la BD
	 */
	private DataSource DS=null;
	
	/**
	 * Constructor que recibe un DataSource como parámetro
	 * @param DS
	 */
	public BeanSQL(DataSource DS)
	{
		this.DS=DS;
	}
	/**
	 * Se encarga de averiguar si un usuario que se le pasa como parametro, 
	 * existe en la tabla de usuarios
	 * @param usuario	Cadena
	 * @return  existe	Booleano
	 * @throws SQLException
	 */
	public boolean existeUsuario(String usuario) throws SQLException
	{
		boolean existe = true;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = "select * from usuarios where usuario = '"+ usuario + "'";
		rs = st.executeQuery(sql);
		if (!rs.next())
		{
			existe=false;
		}
		conexion.close();
		return existe;
	}
	/**
	 * Si existe el usuario y la clave, devuelve el campo nivel de acceso
	 *  si no existe devuelve un null
	 * 
	 * @param usuario	Cadena
	 * @param clave	 	Cadena
	 * @return   nivel   Cadena que puede valer 'candidato' o 'admin'
	 * @throws SQLException
	 */
	public String loginOk(String usuario,String clave) throws SQLException
	{
		String nivel = null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select nivel from usuarios "
				+ "where usuario = '"+ usuario + "' and clave ='"+ clave +"'";
		rs = st.executeQuery(sql);
		if (rs.next())
		{
			nivel=rs.getString("nivel");
		}
		conexion.close();
		return nivel;
	}
	
	/**
	 * Se encarga de insertar los datos de login (usuario y clave) de un candidato que se ha 
	 * registrado en la aplicación.
	 * @param usuario    Cadena con el código de usuario para hacer login
	 * @param clave		Cadena con la clave del usuario
	 * @return  ok  	Valor booleano
	 * @throws SQLException
	 */
	public boolean insertUsuario(String usuario,String clave) throws SQLException
	{
		boolean ok = true;
		Connection conexion = null;
		Statement st = null;
		int inserciones = 0;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"INSERT INTO usuarios (usuario,clave) values ('"+ usuario + "','"+ clave +"')";
		inserciones = st.executeUpdate(sql);
		if (inserciones==0)
		{
			ok=false;
		}
		conexion.close();
		return ok;
	}
	
	/**
	 * Se encarga de insertar en la BD los datos de un candidato, cuyos valores se le pasan
	 * como parámetro a través de un objeto de la clase BeanCandidato
	 * @param candidato  Obejeto de tipo BeanCandidato
	 * @return ok 	 Valor Booleano
	 * @throws SQLException
	 */
	public boolean insertCandidato(BeanCandidato candidato) throws SQLException
	{
		boolean ok = true;
		Connection conexion = null;
		Statement st = null;
		int inserciones = 0;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql="INSERT INTO `candidatos` (`nombre`,`apellidos`,`edad`,`ciudad`,"
				+ "`categoria`,`usuario`) "
				+ "VALUES ('"+candidato.getNombre()+"',"
						+ "'"+candidato.getApellidos()+"',"
						+ Integer.parseInt(candidato.getEdad())+","
						+ "'"+candidato.getCiudad()+"',"
						+ Integer.parseInt(candidato.getCategoria())+","
						+ "'"+candidato.getUsuario()+"')";
		inserciones = st.executeUpdate(sql);
		if (inserciones==0)
		{
			ok=false;
		}
		conexion.close();
		return ok;
	}

	/**
	 * Devuelve un ArrayList de cadenas con las descripciones de categorías de la tabla delç
	 * mismo nombre de la BD
	 * @return categorias    Obejto ArrayList<String>
	 * @throws SQLException
	 * @throws MyExceptionSQL
	 */
	public ArrayList<String> getCategorias() throws SQLException, MyExceptionSQL
	{
		ArrayList<String> categorias=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;	
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql="select * from categorias";
		rs = st.executeQuery(sql);
		
		if (rs!=null)
		{	categorias=new ArrayList<String>();
			while (rs.next())
			{
				 categorias.add(rs.getString("descripcion"));
			}
		}
		else
		{
			throw new MyExceptionSQL("No existe ninguna categoría "
					+ "a la que poder presentarse");
		}
		conexion.close();
		return categorias;
	}

	/**
	 * Obtiene los datos de audición de un candidato cuyo código de usuario se pasa como 
	 * argumento. Luego mapea esos datos a un objeto de tipo 'BeanAudicion' y lo devuelve
	 * 
	 * @param usuario   Cadena con el código de usuario
	 * @return	audicion  Un objeto de tipo BeanAudicion
	 * @throws SQLException
	 */
	public BeanAudicion getAudicion(String usuario) throws SQLException
	{
		BeanAudicion audicion=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select fecha,localidad,lugar from audicion "
				+ "where candidato = '"+ usuario + "'";
		rs = st.executeQuery(sql);
		if (rs.next())
		{
			audicion=new BeanAudicion(rs.getDate("fecha"), rs.getString("localidad"), rs.getString("lugar"));
		}
		conexion.close();
		return audicion;
	}

	/**
	 * Obtiene los datos de un candidato cuyo código de usuario se le pasa como argumento.
	 * Luego mapea dichos dátos a un objeto de tipo 'BeanCandidato' y lo devuelve.
	 * @param usuario  Cadena con el código de usuario
	 * @return candidato  Un objeto de tipo candidato
	 * @throws SQLException
	 */
	public BeanCandidato getCandidato(String usuario) throws SQLException
	{
		BeanCandidato candidato=new BeanCandidato();
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select a.*,b.clave from candidatos a, usuarios b "
				+ "where a.usuario=b.usuario and a.usuario = '"+ usuario + "'";
		rs = st.executeQuery(sql);
		if (rs.next())
		{
			candidato.setNombre(rs.getString("nombre"));
			candidato.setApellidos(rs.getString("apellidos"));
			candidato.setEdad(Integer.toString(rs.getInt("edad")));
			candidato.setCiudad(rs.getString("ciudad"));
			candidato.setCategoria(Integer.toString(rs.getInt("categoria")));
			candidato.setUsuario(rs.getString("usuario"));
			candidato.setClave(rs.getString("clave"));
			candidato.setSeleccionado(rs.getString("seleccionado"));
		}
		conexion.close();
		return candidato;
	}
	
	/**
	 * Devuelve un objeto mapa donde los índices son los códigos de usuario, y 
	 * las claves son los nombres y apellidos reales del usuario, según el parámetro <seleccionado>
	 * @param seleccionado   Debe ser 'S' o 'N' según si se desea obtener los candidatos seleccionados
	 * o no seleccionados
	 * @return  mapa  Un objeto HashMap
	 * @throws SQLException
	 */
	public HashMap<String,String> getSeleccionados(String seleccionado) throws SQLException
	{
		HashMap<String,String> mapa=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select usuario,nombre,apellidos from candidatos "
				+ "where seleccionado = '"+seleccionado+"'";
		rs = st.executeQuery(sql);
		if (rs!=null)
		{
			mapa=new HashMap<String,String>();
			while (rs.next())
			{
				String nombreCompleto=rs.getString("nombre") + " "+ rs.getString("apellidos");
				mapa.put(rs.getString("usuario"),nombreCompleto);
			}
		}
		conexion.close();
		return mapa;
	}

	/**
	 * Devuelve un objeto mapa donde los índices son los nombres de usuario para hacer login, y 
	 * las claves son los nombres y apellidos reales del usuario, según el parámetro <evaluado>
	 * @param evaluado  Se espera que valga 'S' o 'N' según si se desea obtener los candidatos
	 * evaluado o no evaluados
	 * @return  mapa  Un objeto HashMap
	 * @throws SQLException
	 */
	public HashMap<String, String> getEvaluados(String evaluado) throws SQLException
	{
		HashMap<String,String> mapa=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select usuario,nombre,apellidos from candidatos "
				+ "where evaluado = '"+evaluado+"'";
		rs = st.executeQuery(sql);
		if (rs!=null)
		{
			mapa=new HashMap<String,String>();
			while (rs.next())
			{
				String nombreCompleto=rs.getString("nombre") + " "+ rs.getString("apellidos");
				mapa.put(rs.getString("usuario"),nombreCompleto);
			}
		}
		conexion.close();
		return mapa;
	}

	/**
	 * Inserta un periodo de registros en la Base de Datos
	 * 
	 * @param fechaI   	Cadena con una fecha Inicial
	 * @param fechaF	Cadena con una fecha Final
	 * @return  true o false
	 * @throws SQLException
	 * @throws ErrorManejadorFecha
	 */
	public boolean insertPeriodoRegistro(String fechaI, String fechaF) throws SQLException, ErrorManejadorFecha
	{
		boolean ok = true;
		BeanFecha beanFecha=new BeanFecha();
		Connection conexion = null;
		Statement st = null;
		int inserciones = 0;
		String sql;
		conexion = DS.getConnection();
		
		// Borro lo que hubiera en la tabla de `periodoregistro`
		st = conexion.createStatement();
		sql="Delete FROM periodoregistro";
		st.executeUpdate(sql);
		
		// Despues convierto las fechas a cadenas MySQL e inserto
		Date fecha1=beanFecha.parseaFecha(fechaI);
		Date fecha2=beanFecha.parseaFecha(fechaF);
		fechaI=beanFecha.converUtilDateToSqlString(fecha1);
		fechaF=beanFecha.converUtilDateToSqlString(fecha2);
		sql="INSERT INTO periodoregistro (fechaini,fechafin) "
				+ "VALUES ('"+fechaI+"','"+fechaF+"')";
		inserciones = st.executeUpdate(sql);
		if (inserciones==0)
		{
			ok=false;
		}
		conexion.close();
		return ok;
	}
	
	/**
	 * Averigua si ha finalizado el periodo de registro de candidatos
	 * 
	 * @return  true o false
	 * @throws SQLException
	 * @throws MyExceptionSQL
	 */
	public boolean isFinalizadoPeriodoRegistro() 
			throws SQLException, MyExceptionSQL
	{
		boolean ok = true;
		BeanFecha beanFecha=new BeanFecha();
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select fechafin from periodoregistro ";
		rs = st.executeQuery(sql);
		if (rs!=null)
		{
			rs.next();
			Date fechaF=(Date) rs.getDate("fechafin");
			Date hoy=new Date();
			if(beanFecha.esMayorIgual(fechaF, hoy))
			{
				ok=false;
			}
		}
		else
		{
			throw new MyExceptionSQL("Aun no se ha establecido el periodo de registro");
		}
		conexion.close();
		return ok;
	
	}
	/**
	 * Averigua si ha comenzado el periodo de registro
	 * @return ok	Booleano
	 * @throws SQLException
	 * @throws MyExceptionSQL
	 */
	public boolean isComenzadoPeriodoRegistro() 
			throws SQLException, MyExceptionSQL
	{
		boolean ok = true;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"select fechaini from periodoregistro ";
		rs = st.executeQuery(sql);
		if (rs!=null)
		{
			rs.next();
			Date fechaI=(Date) rs.getDate("fechaini");
			Date hoy=new Date();
			// La fecha de inicio es mayor que hoy ?
			if(fechaI.compareTo(hoy)>0)
			{
				ok=false;
			}
		}
		else
		{
			throw new MyExceptionSQL("Aun no se ha establecido el periodo de registro");
		}
		conexion.close();
		return ok;
	}
	
	/**
	 * Establece el valor del campo 'seleccionado' a 'S' o 'N', de la 
	 * tabla de candidatos aspirantes a participar en el concurso
	 * 
	 * @param candidato  Objeto de tipo BeanCandidato
	 * @throws SQLException 
	 * @throws MyExceptionSQL 
	 */
	public void updEvalCandidato(BeanCandidato candidato) 
			throws SQLException, MyExceptionSQL
	{
		Connection conexion = null;
		Statement st = null;
		int inserciones = 0;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql="UPDATE `candidatos` "
				+ " SET seleccionado ='"+ candidato.getSeleccionado()+"'"
				+ ", evaluado ='S' "
				+ "WHERE usuario='"+candidato.getUsuario()+"'";
		inserciones = st.executeUpdate(sql);
		if (inserciones==0)
		{
			throw new MyExceptionSQL("No se ha realizado la actualización"
					+ "del usuario");
		}
		conexion.close();	
	}
	
	/**
	 * Se encarga de establecer el valor correcto en la variable de sesión 
	 * "isPeriodoAbierto"  que puede valer true o false
	 * 
	 * @param sesion	Objeto sesión
	 * @throws SQLException
	 * @throws MyExceptionSQL
	 */
	public void setPeriodoRegAbierto(HttpSession sesion)
			throws SQLException, MyExceptionSQL
	{
		if(isComenzadoPeriodoRegistro() 
				&& !isFinalizadoPeriodoRegistro())
		{
			sesion.setAttribute("isPeriodoAbierto", "true");
		}
		else
		{
			sesion.setAttribute("isPeriodoAbierto", "false");
		}
	}
	public HashMap<String, String> getNoEvaluadosConAudicion() 
			throws SQLException
	{
		HashMap<String,String> mapa=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"SELECT usuario,nombre,apellidos "
				+ "FROM candidatos a, audicion b "
				+ "WHERE a.usuario=b.candidato and evaluado = 'N'";
		rs = st.executeQuery(sql);
		if (rs!=null)
		{
			mapa=new HashMap<String,String>();
			while (rs.next())
			{
				String nombreCompleto=rs.getString("nombre") + " "+ rs.getString("apellidos");
				mapa.put(rs.getString("usuario"),nombreCompleto);
			}
		}
		conexion.close();
		return mapa;
	}
	
	/**
	 * Guarda en la BD una audición asignada a un usuario candidato
	 * 
	 * @param usuario  String con el código de usuario
	 * @param audicion  Objeto del tipo 'Audicion' con los datos de la audición
	 * @return  ok  Booleano
	 * @throws SQLException
	 */
	public boolean setAudicion(String usuario, BeanAudicion audicion) 
			throws SQLException
	{
		boolean ok = true;
		Connection conexion = null;
		Statement st = null;
		int inserciones = 0;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql="INSERT INTO audicion (candidato,fecha,localidad,lugar) "
				+ "VALUES ('"+usuario+"',"
				+ "'"+ new java.sql.Date(audicion.getFecha().getTime()) +"',"
				+ "'"+ audicion.getLocalidad()+"',"
				+ "'"+ audicion.getLugar()+"')";
		inserciones = st.executeUpdate(sql);
		if (inserciones==0)
		{
			ok=false;
		}
		conexion.close();
		return ok;
	}
	
	/**
	 * Devuelve el codigo de usuario, nombre y apellidos de aquellos 
	 * candidatos que aun no tienen establecida una audición
	 * 
	 * @return mapa  Un HashMap con los datos de cada candidato
	 * @throws SQLException
	 */
	public HashMap<String, String> getCandidatosSinAudicion() 
			throws SQLException
	{
		HashMap<String,String> mapa=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql = 	"SELECT usuario,nombre,apellidos "
				+ "FROM candidatos a LEFT JOIN audicion b "
				+ " on usuario=candidato "
				+ " where candidato IS NULL";
		rs = st.executeQuery(sql);
		if (rs!=null)
		{
			mapa=new HashMap<String,String>();
			while (rs.next())
			{
				String nombreCompleto=rs.getString("nombre") + " "+ rs.getString("apellidos");
				mapa.put(rs.getString("usuario"),nombreCompleto);
			}
		}
		conexion.close();
		return mapa;
	}
	
	/**
	 * Devuelve los distintos localidades en los que puede darse una audición
	 * en formato  ArrayList<String> 
	 * 
	 * @return  localidades  ArrayList<String> 
	 * @throws SQLException 
	 * @throws MyExceptionSQL 
	 */
	public HashMap<Integer,String> getLocalidades()
			throws SQLException, MyExceptionSQL
	{
		HashMap<Integer,String> mapa=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql="SELECT codpostal,descripcion FROM localidades";
		rs = st.executeQuery(sql);
		if (rs!=null)
		{
			mapa=new HashMap<Integer,String>();
			while (rs.next())
			{
				mapa.put(rs.getInt("codpostal"),rs.getString("descripcion"));
			}
		}
		conexion.close();
		return mapa;
	}
	
	/**
	 * Devuelve los distintos lugares en los que puede darse una audición
	 * en función de la localidad que se le pasa como parámetro
	 * devolviéndose en formato de array de cadenas
	 * 
	 * @return  lugares   ArrayList<String>
	 * @throws SQLException 
	 * @throws MyExceptionSQL 
	 */
	public ArrayList<String>  getLugares(Integer localidad )
			throws SQLException, MyExceptionSQL
	{
		ArrayList<String> lugares=null;
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;	
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql="SELECT descripcion FROM lugares"
				+ " WHERE codpostal="+localidad;
		rs = st.executeQuery(sql);
		
		if (rs!=null)
		{	lugares=new ArrayList<String>();
			while (rs.next())
			{
				lugares.add(rs.getString("descripcion"));
			}
		}
		else
		{
			throw new MyExceptionSQL("No existe ningun lugar para el codpostal "
					+ localidad);
		}
		conexion.close();
		return lugares;
	}
}
