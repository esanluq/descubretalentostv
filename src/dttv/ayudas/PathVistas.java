package dttv.ayudas;

public class PathVistas
{
	public static String ADMINISTRADOR="/WEB-INF/vistas/administradorW.jsp";
	public static String CANDIDATO="/WEB-INF/vistas/candidatoW.jsp";
	public static String GESERROR="/WEB-INF/vistas/gesErrorW.jsp";
	public static String LOGIN="/WEB-INF/vistas/loginW.jsp";
	public static String REGISTRO="/WEB-INF/vistas/registroW.jsp";
}
