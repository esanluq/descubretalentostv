package dttv.ayudas;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import dttv.modelo.excepciones.ErrorManejadorFecha;


/**
 * Proporciona soporte para entrada y salida implementando el
 * patron Singleton.
 *
 */
public class BeanFecha
{

	/**
	 * Comprueba si una cadena fecha, según el formato dd/MM/yyyy
	 * es una fecha verdadera, y si lo es la devuelve en formato util.Date
	 *  y si no lo es, lanza una excepcion ErrorManejadorFecha
	 *
	 * @param fechaCadena Una cadena con la fecha a parsear en formato dd/MM/yyyy
	 * @return fechaParseada Fecha en formato java.util.Date una vez comprobada su validez
	 * @throws ErrorManejadorFecha Lanza una excepcion si la fecha no es correcta
	 */
	public Date parseaFecha(String fechaCadena) throws ErrorManejadorFecha 
	{
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaDate;
		System.out.println("----->"+fechaCadena);
		// Exigimos un cumplimiento estricto del formato por parte de fecha
		formatoFecha.setLenient(false);
		try
		{
			fechaDate=formatoFecha.parse(fechaCadena.trim());
		}
		catch (ParseException pe)
		{
			throw new ErrorManejadorFecha("La fecha introducida no es correcta");
		}
		return fechaDate;
	}

	/**
	 * Metodo que comprueba si una fecha es mayor o igual que 
	 * otra
	 *
	 * @param fecha1     Fecha1
	 * @param fecha2     Fecha2
	 * @return ok    Valor booleano
	 */
	public boolean esMayorIgual(Date fecha1,Date fecha2)
	{
		boolean ok=false;
		if (fecha1.compareTo(fecha2) >= 0) 
		{   
			ok=true;   
		} 
		return ok;
	}

	/**
	 * Metodo que recibe un objeto java.util.Date y
	 * devuelve una cadena de texto con una fecha en el
	 * formato de fecha que acepta el motor MySQL
	 *
	 * @param utilDate  El objeto java.util.Data
	 * @return cadenaFecha  String con la fecha en el formato MySQL
	 */
	public String converUtilDateToSqlString(Date utilDate)
	{
		SimpleDateFormat sdf;
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		String sqlDate = sdf.format(utilDate);
		return sqlDate;
	}

	/**
	 * Metodo que a partir de un objeto de tipo java.sql.Date
	 * devuelve una cadena con su representacion en la forma local
	 * del sistema.
	 * 
	 * @param fecha   La fecha del tipo java.sql.Date
	 * @return  fechaFormatoLocal   Cadena de texto con la fecha en formato local
	 */
	public String converFechaLocal(java.sql.Date fecha)
	{
		String fechaFormatoLocal;
		fechaFormatoLocal = DateFormat.getDateInstance().format(fecha);
		return fechaFormatoLocal;
	}
	/**
	 * Metodo sobrecargado que a partir de un objeto de tipo java.util.Date
	 * devuelve una cadena con su representacion en la forma local
	 * del sistema.
	 * 
	 * @param fecha   La fecha del tipo java.util.Date
	 * @return  fechaFormatoLocal   Cadena de texto con la fecha en formato local
	 */
	public String converFechaLocal(Date fecha)
	{
		String fechaFormatoLocal;
		fechaFormatoLocal = DateFormat.getDateInstance().format(fecha);
		return fechaFormatoLocal;
	}
	
	/**
	 * Convierte de java.sql.Date a java.util.Date
	 * 
	 * @param fecha  Fecha en formato util.Date
	 * @return Fecha de tipo sql.Date
	 */
	public java.sql.Date toSqlDate(java.util.Date fecha)
	{
		return new java.sql.Date(fecha.getTime());
	}
}
