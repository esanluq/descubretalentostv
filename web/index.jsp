<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isErrorPage="true"%>
<%@ page session='true' %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="es" />

    <meta name="copyright" content="Design/Code: Vit Dlouhy [Nuvio - www.nuvio.cz]; e-mail: vit.dlouhy@nuvio.cz" />
    
    <title>DESCUBRE TALENTOS RTVE</title>
    <meta name="description" content="Seleccion de Talentos para Concurso de Television" />
    <meta name="keywords" content=" concurso, audicion, talentos, tv" />
    
    <link rel="index" href="./" title="Inicio" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/principal.css" />
    <script type="text/javascript" src="scripts/actions.js"></script>
</head>

<body>
<form name="formu" action="controlador" method="post" >
<input type="hidden" name="accion" value=""/>
<!-- Contenedor -->
<div id="contenedor">

    <!-- Cabecera -->
    <div id="cabecera">

        <!-- Logo -->
        <h1 id="logo">DTTV - DESCUBRE TALENTOS <span id="rtve">RTVE</span></h1>
		<div class="clear"></div>
    </div> <!-- /cabecera -->

     <!-- Menu principal -->
     <div id="menu">
            <ul>
                <li class="seleccionado"><a onclick="submitAction('inicio');">Inicio</a></li>
				<li><a onclick="submitAction('formLogin');">Login</a></li>
				<li><a onclick="submitAction('formRegistrar');">Registrarse</a></li>

            </ul>

        <div class="clear"></div>
     </div> <!-- /menu principal -->

     <!-- Contenido -->
     <div id="contenido">

		<!-- Principal -->
		<div id="principal">

            <!-- Articulo -->
            <div class="articulo">
                <h2>DTTV - DESCUBRE TALENTOS <cite>RTVE</cite> </h2>

                <p> Sean bienvenidos a la Web del programa DTTV, del Segundo Canal de RTVE. En esta web,
                 aquellas personas que piensan que tienen algo que decir en las artes plásticas: Canción, Baile, Destreza o Humor, 
                 tienen su oportunidad de saltar a la fama.<br/>
                 Puede usted Registrarse en una de estas categorías, y si pasa el casting, 
                 participará en nuestro afamado programa de Televisión.<br/>
                 No pierda esta oportunidad!.
                 <br/>
                 Le saludamos atentamente la dirección de este programa "DTTV"</p>

            </div> <!-- /articulo -->

            <div class="clear"></div>
            
        </div><!-- /principal -->

        <!-- Secundario -->
        <div id="secundario">

                <!-- Sobre mi -->
                <h3>&nbsp;</h3>

                <div id="sobremi">
                    <p><strong></strong></p>
                </div> <!-- /sobre mi -->

                <div class="clear"></div>

                <div class="clear"></div>

        </div> <!-- /secundario -->

	<div class="clear"></div>
    </div> <!-- /contenido -->

    <!-- Pie de pagina -->
    <div id="pie">
        <p id="copyright">&copy; DTTV RTVE</p>
    </div> <!-- /pie de pÃƒÂ¡gina -->

</div> <!-- /contenedor -->
	</form>
</body>
</html>