<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isErrorPage="true"%>
<%@ page import="dttv.modelo.beans.BeanMenuAdmin"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
    <!-- Este disenio esta basado en un disenio web libre llamado CrystalX y que se puede descargar desde
         la direccion http://www.oswd.org/design/preview/id/3465 -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="es" />

    <meta name="copyright" content="Design/Code: Vit Dlouhy [Nuvio - www.nuvio.cz]; e-mail: vit.dlouhy@nuvio.cz" />
    
    <title>DESCUBRE TALENTOS RTVE</title>
    <meta name="description" content="Seleccion de Talentos para Concurso de Television" />
    <meta name="keywords" content=" concurso, audicion, talentos, tv" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/principal.css" />
    <script type="text/javascript" src="./scripts/actions.js"></script>
</head>

<body>
<%
		BeanMenuAdmin beanMenuAdmin = (BeanMenuAdmin) request.getAttribute("modelo");
		HttpSession sesion=request.getSession();
%>
<form name="formu" action="controlador" method="post">
<input type="hidden" name="accion" value=""/>
<input type="hidden" name="procedencia" value="administradorW"/>
<input type="hidden" name="usuario" value=""/>

<!-- Contenedor -->
<div id="contenedor">

    <!-- Cabecera -->
    <div id="cabecera">

        <!-- Logo -->
        <h1 id="logo">DTTV - DESCUBRE TALENTOS <span id="rtve">RTVE</span></h1>
		<div class="clear"></div>
    </div> <!-- /cabecera -->

     <!-- Menu principal -->
     <div id="menu">
        <div class="clear"></div>
     </div> <!-- /menu principal -->

     <!-- Contenido -->
     <div id="contenido">

		<!-- Principal -->
		<div id="principal">

            <!-- Articulo -->
            <div class="articulo">
                <h2>Area de Administración</h2>
                
            </div> <!-- /articulo -->

             <div style="margin-left: 10%;" ><%=beanMenuAdmin.getContenido() %></div>
            
        </div><!-- /principal -->

        <!-- Secundario -->
        <div id="secundario">

                <!-- Sobre mi -->
                <h3>Sesión Administración</h3>

                <div id="sobremi">
                    <p>Bienvenido <strong><%=sesion.getAttribute("usuario") %></strong>
						<a class="botonLogout" onclick="submitAction('logout');">Logout</a>
                    </p>
                </div> <!-- /sobre mi -->

                <div class="clear"></div>

                <!-- Categorias -->
                <h3>Operaciones Candidatos</h3>
                <ul id="opciones">
                	<li><a href="#" onclick="submitAction('formSetAudicion');">Establecer Audición</a></li>
                  	<li><a href="#" onclick="submitAction('formEvalCandidato');">Evaluar</a></li>
                    <li><a href="#" onclick="submitAction('verSeleccionados');">Ver Seleccionados</a></li>
                    <li><a href="#" onclick="submitAction('verNoSeleccionados');">Ver No Seleccionados</a></li>
                    <li><a href="#" onclick="submitAction('verPendientesEval');">Ver Pendientes Evaluar</a></li>
                </ul>
                <h3>Otras Operaciones</h3>
                <ul id="opciones">
                    <li><a href="#" onclick="submitAction('verFormPeriodoReg');">Establecer Periodo Registro</a></li>
                    <li><a href="#" onclick="submitAction('verCategorias');">Ver Categorías</a></li>
                </ul>
                <div class="clear"></div>

        </div> <!-- /secundario -->

	<div class="clear"></div>
    </div> <!-- /contenido -->

    <!-- Pie de pagina -->
    <div id="pie">
        <p id="copyright">&copy; DTTV RTVE</p>
    </div> <!-- /pie de pagina -->

</div> <!-- /contenedor -->
</form>
</body>
</html>
