<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isErrorPage="true"%>
<%@ page import="dttv.modelo.beans.BeanRegistro"%>
<%@ page import="dttv.modelo.beans.BeanCandidato"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
    <!-- Este disenio esta basado en un disenio web libre llamado CrystalX y que se puede descargar desde
         la direccion http://www.oswd.org/design/preview/id/3465 -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="es" />

    <meta name="copyright" content="Design/Code: Vit Dlouhy [Nuvio - www.nuvio.cz]; e-mail: vit.dlouhy@nuvio.cz" />
    
    <title>DESCUBRE TALENTOS RTVE</title>
    <meta name="description" content="Seleccion de Talentos para Concurso de Television" />
    <meta name="keywords" content=" concurso, audicion, talentos, tv" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/principal.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/secundario.css" />
    <script type="text/javascript" src="./scripts/actions.js"></script>
    <script type="text/javascript" src="./scripts/validarRegistro.js"></script>
</head>

<body>
<%
		BeanRegistro beanRegistro = (BeanRegistro) request.getAttribute("modelo");
		BeanCandidato beanCandidato=beanRegistro.getBeanCandidato();
%>
<form name="formu" action="controlador" method="post" onsubmit="return validar();">
	<input type="hidden" name="accion" value=""/>

<!-- Contenedor -->
<div id="contenedor">

    <!-- Cabecera -->
    <div id="cabecera">

        <!-- Logo -->
        <h1 id="logo">DTTV - DESCUBRE TALENTOS <span id="rtve">RTVE</span></h1>
		<div class="clear"></div>
    </div> <!-- /cabecera -->

     <!-- Menu principal -->
     <div id="menu">
            <ul>
                <li><a onclick="submitAction('inicio');">Inicio</a></li>
				<li><a onclick="submitAction('formLogin');">Login</a></li>
                <li class="seleccionado"><a onclick="submitAction('formRegistrar');">Registrarse</a></li>
            </ul>

        <div class="clear"></div>
     </div> <!-- /menu principal -->

     <!-- Contenido -->
     <div id="contenido">

		<!-- Principal -->
		<div id="principal">

            <!-- Articulo -->
            <div class="articulo">
                <h2>Inscripción Solicitudes</h2>
                  	<div id="login">
	                	<ul>
							<li>
								<label for="usuario">Usuario:</label> 
								<input type="text" id="usuario" name="usuario" value="<%=beanCandidato.getUsuario() %>"/> 
							 	<span class="msgerror"> 
									<%=beanCandidato.getErrorUsuario() %>
								</span><br/>
							</li>
							<li>
								<label for="clave">Clave:</label> 
									<input type="password" id="clave" name="clave" value="<%=beanCandidato.getClave() %>"/> 
								<span class="clave"> 
							 		<%=beanCandidato.getErrorClave() %>
								</span> 
							</li>
							<li>
								<label for="nombre">Nombre:</label> 
								<input type="text" id="nombre" name="nombre" value="<%=beanCandidato.getNombre() %>"/> 
							</li>
							<li>
								<label for="apellidos">Apellidos:</label> 
									<input type="text" id="apellidos" name="apellidos" value="<%=beanCandidato.getApellidos() %>"/> 
							</li>
							<li>
								<label for="edad">Edad:</label> 
								<input type="text" id="edad" name="edad" value="<%=beanCandidato.getEdad() %>"/> 
							</li>
							<li>
								<label for="ciudad">Ciudad:</label> 
									<input type="text" id="ciudad" name="ciudad" value="<%=beanCandidato.getCiudad() %>"/> 
							</li>
							<li>
								<label for="categoria">Categoría:</label> 
									<%=beanRegistro.getComboCategorias()%>
							</li>
						</ul>
						<div >
							<input type="submit" class="botonLogin" value="Enviar"/> 
							<input type="reset" class="botonLogin" />
						</div>
					</div>
            </div> <!-- /articulo -->

            <div class="clear"></div>
            
        </div><!-- /principal -->

        <!-- Secundario -->
        <div id="secundario">

                <!-- Sobre mi -->
                <h3>&nbsp;</h3>

                <div id="sobremi">
                    <p><strong></strong></p>
                </div> <!-- /sobre mi -->

                <div class="clear"></div>

                <div class="clear"></div>

        </div> <!-- /secundario -->

	<div class="clear"></div>
    </div> <!-- /contenido -->

    <!-- Pie de pagina -->
    <div id="pie">
        <p id="copyright">&copy; DTTV RTVE</p>
    </div> <!-- /pie de pagina -->

</div> <!-- /contenedor -->
</form>
</body>
</html>
