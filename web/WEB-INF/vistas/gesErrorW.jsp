<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isErrorPage="true" %>
<%@ page import="dttv.modelo.beans.BeanError" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
    <!-- Este diseño está basado en un diseño web libre llamado CrystalX y que se puede descargar desde
         la dirección http://www.oswd.org/design/preview/id/3465 -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="es" />

    <meta name="copyright" content="Design/Code: Vit Dlouhy [Nuvio - www.nuvio.cz]; e-mail: vit.dlouhy@nuvio.cz" />
    
    <title>DESCUBRE TALENTOS RTVE</title>
    <meta name="description" content="Seleccion de Talentos para Concurso de Television" />
    <meta name="keywords" content=" concurso, audicion, talentos, tv" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/principal.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/secundario.css" />
    <script type="text/javascript" src="./scripts/actions.js"></script>
</head>

<body>
<%
	BeanError err = (BeanError) request.getAttribute("error");
	HttpSession sesion=request.getSession();
%> 
<form name="formu" action="controlador" method="post" >
<input type="hidden" name="accion" value=""/>
<!-- Contenedor -->
<div id="contenedor">

    <!-- Cabecera -->
    <div id="cabecera">

        <!-- Logo -->
        <h1 id="logo">DTTV - DESCUBRE TALENTOS <span id="rtve">RTVE</span></h1>
		<div class="clear"></div>
    </div> <!-- /cabecera -->

     <!-- Menú principal -->
     <div id="menu">
        <div class="clear"></div>
     </div> <!-- /menú principal -->

     <!-- Contenido -->
     <div id="contenido">

		<!-- Principal -->
		<div id="principal">

            <!-- Articulo -->
            <div class="articulo">
                <h2><a href="#">Error!</a></h2>
                <p> 
                	Lo sentimos, se ha producido un error inesperado.<br/>
					Mensaje de Error: <%=err.getMensError()%> <br/> 
					Mensaje de Excepcion: <%=err.getExcepcion().getMessage() %>
				</p>
				<div >
					<input type="submit" class="botonLogin" name="accion" value="Inicio" /> 
				</div>
            </div> <!-- /articulo -->
			
            <div class="clear"></div>
            
        </div><!-- /principal -->

        <!-- Secundario -->
        <div id="secundario">
        		 <!-- Sobre mi -->
                <h3>&nbsp;</h3>

                <div id="sobremi">
                	<% 
                		if(sesion.getAttribute("login")=="OK")
                		{                	
                	%>
                     <p>Bienvenido <strong><%=sesion.getAttribute("usuario") %></strong></p>
                     <% } %>
                </div> <!-- /sobre mi -->

                <div class="clear"></div>

                <div class="clear"></div>
        </div> <!-- /secundario -->

	<div class="clear"></div>
    </div> <!-- /contenido -->

    <!-- Pie de página -->
    <div id="pie">
        <p id="copyright">&copy; DTTV RTVE</p>
    </div> <!-- /pie de página -->

</div> <!-- /contenedor -->
	</form>
</body>
</html>