function validar()
{
	if(document.getElementById("usuario").value.length==0
		|| document.getElementById("clave").value.length==0 
			|| document.getElementById("nombre").value.length==0
				|| document.getElementById("apellidos").value.length==0
					|| document.getElementById("edad").value.length==0
						|| document.getElementById("ciudad").value.length==0)
	{
		alert("Rellene todos los campos del formulario");
		return false;
	}
	if (isNaN(document.getElementById("edad").value) 
			|| document.getElementById("edad").value<18) 
	{
		alert("Edad debe ser un número mayor o igual a 18");
		return false;
	}
	document.formu.accion.value='validarRegistro';
	return true;
}